<?php
namespace Fuel\Migrations;

class Create_Users
{

    function up()
    {
        \DBUtil::create_table('users', array(
			'id' => array('type' => 'int', 'constraint' => 5,'auto_increment' => true),
			'name' => array('type' => 'varchar', 'constraint' => 100),
			'age' => array('type' => 'int'),
		), array('id'));
    }

    function down()
    {
      \DBUtil::drop_table('users');
    }
}