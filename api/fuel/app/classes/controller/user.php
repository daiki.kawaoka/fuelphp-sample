<?php
class Controller_User extends Controller_Rest
{

	protected $format = 'json';

	public function get_index()
	{
		$users = Model_User::find_all();
        $this->response($users ,200 ,JSON_UNESCAPED_UNICODE);
	}

	public function get_show()
	{
        $user = Model_User::find_by_pk($this->param('id'));
        if($user) {
            $this->response($user, 200, JSON_UNESCAPED_UNICODE);
        }else{
            $this->response(['message' => "ユーザ取得失敗", 'error_message' => 'idが' . $this->param('id') . 'のUserは存在しません'], 404, JSON_UNESCAPED_UNICODE);
        }
    }

    public function post_create()
    {
        $val = Validation::forge();
        $val->add('name', 'Your name')->add_rule('required');
        $val->add('age', 'Your age')->add_rule('required')
            ->add_rule('numeric_min', 0)
            ->add_rule('numeric_max', 130);

        if($val->run()){
            $user = Model_User::forge()->set(array(
                'name' => Input::post('name'),
                'age' => Input::post('age')
            ));

            $user->save();
            $this->response(['message' => "ユーザ作成成功",'user' => $user],200, JSON_UNESCAPED_UNICODE);
        }else{
            $this->response($val->error(), 500, JSON_UNESCAPED_UNICODE);
        }
    }

    public function post_update()
    {
        $val = Validation::forge();
        $val->add('id', 'Your id')->add_rule('required');
        $val->add('name', 'Your name')->add_rule('required');
        $val->add('age', 'Your age')->add_rule('required')
            ->add_rule('numeric_min', 0)
            ->add_rule('numeric_max', 130);

        if($val->run()){
            $user = Model_User::find_by_pk(Input::post('id'));
            $user->name = Input::post('name');
            $user->age = Input::post('age');

            $user->save();
            $this->response(['message' => "ユーザ編集成功",'user' => $user], 200, JSON_UNESCAPED_UNICODE);
        }else{
            $this->response(['message' => "ユーザ編集失敗",$val->error()], 500, JSON_UNESCAPED_UNICODE);
        }
    }

    public function post_delete()
    {
        $val = Validation::forge();
        $val->add('id', 'Your id')->add_rule('required');

        if($val->run()){
            $user = Model_User::find_by_pk(Input::post('id'));
            if($user){
                $user->delete();
                $this->response(['message' => "ユーザ削除成功"],200, JSON_UNESCAPED_UNICODE);
            }else{
                $this->response(['message' => "ユーザ削除失敗",'error_message' => 'idが' . Input::post('id') . 'のUserは存在しません'], 500, JSON_UNESCAPED_UNICODE);
            }
        }else{
            $this->response(['message' => "ユーザ削除失敗",$val->error()], 500, JSON_UNESCAPED_UNICODE);
        }

    }

}
