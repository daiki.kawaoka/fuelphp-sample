<?php
//class Model_User extends \Orm\Model
class Model_User extends \Model_Crud
{
    protected static $_table_name = 'users';
    protected static $_primary_key = 'id';
	protected static $_properties = array(
		"id",
		"name",
		"age",
	);
}
