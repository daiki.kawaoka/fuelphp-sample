<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.9-dev
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */

return array(
	'_root_' => 'welcome/index',
	'_404_' => 'welcome/404',
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
	'api/users' => 'user/index',
    'api/users/create' => 'user/create',
    'api/users/update' => 'user/update',
    'api/users/delete' => 'user/delete',
    'api/users/(:id)' => 'user/show',
);
